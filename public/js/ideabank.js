$(function(){

    var $grid = $('.grid').isotope({
        itemSelector: '.grid-element',
        layoutMode: 'fitRows',
        fitRows: {
            gutter: 5
        },
        getSortData: {
            vote: '.vote parseInt',
        },
        sortBy: 'vote',
        sortAscending: false
    });

    $('button.delete').click(function(){

        var $id = $(this).attr('value');
        var $el = $(this);

        var $r = confirm('Olet laittamassa idean roskikseen \nhaluatko varmasti tehdä tämän? :/');

        if($r == true){
            $.post(
                '/remove',
                { 'id' : $id },
                function(){
                    $el.closest('div.idea-container').remove('slow');
                }
            );
        }

    });

    $('button.vote').click(function(){

        var $id = $(this).attr('value');

        $.post(
            '/vote',
            { 'id' : $id },
            function(){
                $oldval = parseInt($('span.votes'+$id).html());
                $('span.votes'+$id).html( $oldval + 1 )
                $grid.isotope('updateSortData').isotope({ sortBy: 'vote' });
            }
        );
    });

    $('button.done').click(function(){

        var $id = $(this).attr('value');
        var $el = $(this);

        $.post(
            '/done',
            { 'id' : $id },
            function(){
                $el.closest('div.idea-contaner').hide('slow');
            }
        );
    });
});
