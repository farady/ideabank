<?php

class IdeaBankTest extends TestCase
{
    /**
     * @test
     */
    public function addIdea()
    {
        $this->post('/add',
            [
                'token' => 'gd9FTbZjmRRrGQWx820OeXd6',
                'user_name' => 'teppo',
                'text' => 'this is a incoming webhook testing from development environment tests! and seems working.'
            ]
        )->seeStatusCode(200);
    }
}
