<?php

$app->post('/add', ['middleware' => ['slacktoken'], 'uses' => 'ApiController@add']);

$app->group(['middleware' => 'auth', 'namespace' => 'App\Http\Controllers'], function($app){

    // frontpage actions
    $app->get('/', ['uses' => 'BankController@showIdeas']);
    $app->get('/trashcan', ['uses' => 'BankController@showRemoved']);
    $app->get('/done', ['uses' => 'BankController@showDone']);
    $app->post('/remove', ['uses' => 'BankController@remove']);
    $app->post('/vote', ['uses' => 'BankController@vote']);
    $app->post('/done', ['uses' => 'BankController@done']);

});

$app->get('/login', function() {
    return view('idea.login');
});

$app->post('/login', ['middleware' => 'csrf' , 'uses' => 'AuthController@handleLogin']);