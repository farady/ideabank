<?php namespace App\Http\Middleware;

use Closure;

class VerifySlackToken
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->input('token');

        if($token != $_ENV['IDEABANK_TOKEN'])
        {
            abort(403, 'Authorization token mismatch');
        }

        return $next($request);
    }
}