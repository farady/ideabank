<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class AuthController extends BaseController
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View|\Laravel\Lumen\Http\Redirector
     */
    public function handleLogin(Request $request)
    {
        $password = $request->input('password');
        $username = $request->input('username');

        if( password_verify($password, env('IDEABANK_PASSWORD')) &&
            env('IDEABANK_USERNAME') == $username)
        {
            $request->session()->set('authenticated', true);
            return redirect('/');
        }
        else
        {
            $request->session()->flash('msg', 'Wrong username or bad password');
            return view('idea.login');
        }
    }
}