<?php namespace app\Http\Controllers;

use DB;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class ApiController extends BaseController
{

    /**
     * @param Request $request
     * @return string
     */
    public function add(Request $request)
    {
        $name =  $request->input('user_name');
        $idea = $request->input('text');

        $body = json_encode(
            [
                'text' => "User: $name\nAdded new idea:\n$idea"
            ]
        );

        DB::beginTransaction();
        try {
            # save to ideabank
            DB::table('ideabank')->insert(
                [
                    'name' => $name,
                    'idea' => $idea
                ]
            );

            # respond to slack
            $client = new Client();
            $client->request('POST', $_ENV['SLACK_HOOK_URL'], ['body' => $body]);
        }
        catch(\Exception $e)
        {
            DB::rollBack();
            return 'Something went horribly wrong and your idea vanished into thin air!';
        }

        DB::commit();
        return 'successfully added new idea!';
    }
}