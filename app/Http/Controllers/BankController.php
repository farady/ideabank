<?php namespace App\Http\Controllers;

use \DB;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class BankController extends BaseController
{
    /**
     * @return \Illuminate\View\View
     */
    public function showIdeas()
    {
        $ideas = DB::table('ideabank')
            ->whereNull('deleted_at')
            ->whereNull('done_at')
            ->orderBy('votes','desc')
            ->get();

        return view('idea.list', [ 'ideas' => $ideas ]);
    }

    /**
     * @param Request $request
     */
    public function done(Request $request)
    {
        $this->validate(
            $request,
            [
                'id' => 'integer'
            ]
        );

        $id = $request->input('id');
        $numberAffected = DB::table('ideabank')
            ->where('id', '=', $id)
            ->update(['done_at' => DB::raw('NOW()')]);

        return $numberAffected;
    }

    /**
     *
     */
    public function showDone()
    {
        $ideas = DB::table('ideabank')
            ->whereNotNull('done_at')
            ->orderBy('done_at','desc')
            ->get();

        return view('idea.done', [ 'ideas' => $ideas ]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function remove(Request $request)
    {
        $this->validate(
            $request,
            [
                'id' => 'integer'
            ]
        );

        $id = $request->input('id');
        $numberAffected = DB::table('ideabank')
            ->where('id', '=', $id)
            ->update(['deleted_at' => DB::raw('NOW()')]);

        return $numberAffected;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function vote(Request $request)
    {
        $this->validate(
            $request,
            [
                'id' => 'integer'
            ]
        );

        $id = $request->input('id');
        $numberAffected = DB::table('ideabank')
            ->where('id', '=', $id )
            ->increment('votes');

        return $numberAffected;
    }



    /**
     * @return \Illuminate\View\View
     */
    public function showRemoved()
    {
        $ideas = DB::table('ideabank')
            ->whereNotNull('deleted_at')
            ->orderBy('deleted_at','desc')
            ->get();

        return view('idea.trash', [ 'ideas' => $ideas ]);
    }
}
