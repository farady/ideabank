<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDoneColumnToIdeabank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ideabank', function (Blueprint $table) {
            $table->timestamp('done_at')->nullable()->default(DB::raw('NULL'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ideabank', function (Blueprint $table) {
            $table->dropColumn('done_at');
        });
    }
}
