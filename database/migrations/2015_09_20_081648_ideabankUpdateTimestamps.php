<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IdeabankUpdateTimestamps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ideabank', function (Blueprint $table) {
            $table->dropTimestamps();
        });

        Schema::table('ideabank', function (Blueprint $table) {
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ideabank', function (Blueprint $table) {
            $table->dropColumn('created_at');
        });

        Schema::table('ideabank', function (Blueprint $table) {
            $table->timestamps();
        });
    }
}
