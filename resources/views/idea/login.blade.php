@extends('base')

@section('content')
    <div class="container login">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Kirjaudu</h3>
                    </div>
                    <div class="panel-body">
                        <!-- show flash messages here -->
                        @if(Session::has('msg'))
                            <p class="errors">
                                {{ Session::get('msg') }}
                            </p>
                        @endif
                        <form action="login" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <fieldset>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                                    <input name="username" type="text" placeholder="ktunnus" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                                    <input type="password" placeholder="salakala" name="password" class="form-control" />
                                </div>
                            </div>
                            <input type="submit" value="kirjaudu" class="form-control" />
                        </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
