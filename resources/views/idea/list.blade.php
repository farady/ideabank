@extends('base')

@section('content')
    <div class="container">
        @include('menu')
        <div class="grid">
            @foreach( $ideas as $idea )
                <div class="idea-container grid-element panel panel-default">
                    <div class="panel-heading">
                        <span class="idea-id"># {{ $idea->id }}</span>
                        <span class="idea-user pull-right">{{ $idea->name }}</span>
                    </div>
                    <div class="panel-body">
                        <p class="idea-text">{{ $idea->idea }}</p>
                        <p class="idea-timestamp">{{ $idea->created_at }}</p>
                        <p class="idea-timestamp">{{ $idea->done_at }}</p>
                    </div>
                    <div class="panel-footer">
                        <span class="votes{{ $idea->id }} vote">{{ $idea->votes }}</span>
                        <div class="buttons pull-right">
                            <button class="btn-success vote" value="{{ $idea->id }}" >
                                <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
                                {{ trans('translations.vote') }}
                            </button>
                            <button class="btn-warning done" value="{{ $idea->id }}" >
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                {{ trans('translations.implemented') }}
                            </button>
                            <button class="btn-danger delete" value="{{ $idea->id }}" >
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                {{ trans('translations.delete') }}
                            </button>
                        </div>
                    </div>

                </div>
            @endforeach
        </div>
    </div>
@endsection