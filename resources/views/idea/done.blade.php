@extends('base')

@section('content')
    <div class="container">
        @include('menu')
        <div class="grid">
        @foreach( $ideas as $idea )
            <div class="idea-container grid-element panel panel-default">
                <div class="panel-heading">
                    <span class="idea-id"># {{ $idea->id }}</span>
                    <span class="idea-user pull-right">{{ $idea->name }}</span>
                </div>
                <div class="panel-body">
                    <p class="idea-text">{{ $idea->idea }}</p>
                    <p class="idea-timestamp">{{ $idea->created_at }}</p>
                    <p class="idea-timestamp">{{ $idea->done_at }}</p>
                </div>
                <div class="panel-footer">
                    <p class="votes{{ $idea->id }}">{{ $idea->votes }}</p>
                </div>

            </div>
        @endforeach
        </div>
    </div>
@endsection