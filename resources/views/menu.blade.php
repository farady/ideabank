<h3 class="text-muted"><strong>Idea Pankki</strong></h3>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">
                <img  height="100%" alt="Brand" src="https://solinor.fi/images/logo-solinor@2x.png">
            </a>
        </div>
        <ul class="nav navbar-nav">
            <li>
                <a href="/trashcan">{{ trans('translations.deleted') }}</a>
            </li>
            <li>
                <a href="/done">{{ trans('translations.implemented') }}</a>
            </li>
        </ul>
    </div>
</nav>