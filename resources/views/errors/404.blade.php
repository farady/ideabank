@extends('base')

@section('content')

    <div class="container">
        <div class="jumbotron">
            <h2>Whoops, not page you are looking for!</h2>
            <p>404</p>
        </div>
    </div>

@endsection