<?php

return [

    'username'      => 'käyttäjätunnus',
    'idea'          => 'idea',
    'created'       => 'luotu',
    'delete'        => 'poista',
    'vote'          => 'äänestä',
    'votes'         => 'ääniä',
    'deleted'       => 'poistettu',
    'implemented'   => 'toteutettu',
    'about'         => 'meistä',
    'done'          => 'valmis'
];